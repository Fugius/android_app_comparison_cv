# Android_App_Comparison_CV

## What is this repo
A personal challenge inspired by a reddit post : https://www.reddit.com/r/computervision/comments/z4fljv/what_is_a_good_approach_to_find_the_similarity/  

Whith two android apps screenshots, how to determine if they belong to the same app / view ?   
Nothing overcomplicated, works with the example given in the reddit post  

## How does it work ?
First the image is blurred and binarized  
Then the region of interests are found with two simple and fast cumulative histogram.

    - first create a "vertical" cumulative histogram
    - normalize it so the min is 0
    - create an image m0 completely black with the shape of the original image and fill the columns containing informations (given by the cumulative histogram)

    - create an "horizontal" cumulative histogram
    - normalize it so the min is 0
    - create an image m1 completely black with the shape of the original image and fill the rows containing informations (given by the cumulative histogram)

The intersection between m1 and m2 is a "feature mask" indicating where the information is on the image  
Use a "close" filter on the masks to remove noise  

Find connected components -> gives you the bounding box of each feature  

Finaly compare each features of image 1 with each features from image 2.  

For that you could use any CBIR method depending on your images (color histogram, CNN, BOW ...)  
I used a simple distance function for the logos and a mix of OCR and nlp that works surprisingly well for text  


/!\ far from perfect but i'm pretty happy with the result considering i only invested a couple hours into this project.  

## Files
  
    - 1.png & 2.png : reddit example data
    - compare.py : the algorithm (heavily commented)
    - matchs : features matching between the two images
    - no_match : features that didn't match between the two pictures
    - mask1.png & mask2.png : shows you the features mask

## Output / parameters
Outputs the number of features matching against the total number of features found  

parameters you can adjust in the code :  

    - the size of all filters
    - the distance threshold used to decide if two features match (image & text)
    - the name of the input images
