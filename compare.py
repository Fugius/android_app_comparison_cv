#libs : opencv, numpy, tqdm
#for text part : easyocr, nltk, gensim, scikit-learn

import cv2 as cv
import numpy as np
import easyocr
from nltk.tokenize import word_tokenize
from tqdm import tqdm
import gensim
import gensim.downloader
from sklearn.metrics.pairwise import cosine_similarity

# import nltk
# nltk.download('punkt')

IMG_PATH_1 = "1.png"
IMG_PATH_2 = "2.png"

#Loading imgs & converting them to grayscale
img1 = cv.imread(IMG_PATH_1, cv.IMREAD_GRAYSCALE)
img2 = cv.imread(IMG_PATH_2, cv.IMREAD_GRAYSCALE)

#img "denoizong" / binarization
blur_1 = cv.GaussianBlur(img1,(5,5),0)
_, img1 = cv.threshold(blur_1,0,255,cv.THRESH_BINARY+cv.THRESH_OTSU)

blur_2 = cv.GaussianBlur(img2,(5,5),0)
_, img2 = cv.threshold(blur_2,0,255,cv.THRESH_BINARY+cv.THRESH_OTSU)

#removing header and footer
img1[0:150, :] = 0
img2[0:150, :] = 0

img1[-50:-1, :] = 0
img2[-50:-1, :] = 0

#vertical cumulative histogram
vertical_histo_1 = np.sum(img1, axis=0)
vertical_histo_2 = np.sum(img2, axis=0)

#normalize histos and get "column object positions"
vertical_histo_1 = vertical_histo_1 - np.min(vertical_histo_1)
vertical_histo_2 = vertical_histo_2 - np.min(vertical_histo_2)

def getHorizontalPositon(histo):
    pos = []
    for i in range(1, len(histo)):
        if histo[i - 1] == 0 and histo[i] > 0 or histo[i - 1] > 0 and histo[i] == 0:
            pos.append(i)
    return pos

columns1 = getHorizontalPositon(vertical_histo_1)
columns2 = getHorizontalPositon(vertical_histo_2)

#horizontal cumulative histogram
horizontal_histo_1 = np.sum(img1, axis=1)
horizontal_histo_2 = np.sum(img2, axis=1)

#normalize histos and get "row object positions"
horizontal_histo_1 = horizontal_histo_1 - np.min(horizontal_histo_1)
horizontal_histo_2 = horizontal_histo_2 - np.min(horizontal_histo_2)

def getVerticalPositon(histo):
    pos = []
    for i in range(1, len(histo)):
        if histo[i - 1] == 0 and histo[i] > 0 or histo[i - 1] > 0 and histo[i] == 0:
            pos.append(i)
    return pos

rows1 = getVerticalPositon(horizontal_histo_1)
rows2 = getVerticalPositon(horizontal_histo_2)

#Create features mask (text & logos)
def getFeaturesMask(rows, columns, shape):
    mask = np.zeros(shape)

    for i in tqdm(range(0, len(rows), 2)):
        for j in range(0, len(columns), 2):

            y1 = columns[j]
            y2 = columns[j+1]

            x1 = rows[i]
            x2 = rows[i+1]

            mask[x1:x2, y1:y2] = 255
    
    return mask

mask1 = np.uint8(getFeaturesMask(rows1, columns1, img1.shape))
mask2 = np.uint8(getFeaturesMask(rows2, columns2, img2.shape))

#closing to remove noise inside mask
mask1 = cv.morphologyEx(mask1, cv.MORPH_CLOSE, np.ones((5,5),np.uint8))
mask2 = cv.morphologyEx(mask2, cv.MORPH_CLOSE, np.ones((5,5),np.uint8))

#the job is finished for u/MachineLearnz, if you want to keep only logos, only keep the 2 first "horizontal positons"
cv.imwrite("mask1.png", mask1)
cv.imwrite("mask2.png", mask2)

# extracting features of img1 & 2
def extract_features(mask, img):
    features = []

    (numLabels, labels_mask, _, _) = cv.connectedComponentsWithStats(mask, 4, cv.CV_32S)
    
    #for each feature
    for id in tqdm(range(1, numLabels)):
        (numLabels, labels_mask, _, _) = cv.connectedComponentsWithStats(mask, 4, cv.CV_32S)
        labels_mask[labels_mask != id] = 0

        #find position
        col = np.sum(labels_mask, 0)
        y0 = np.min(np.nonzero(col))
        y1 = np.max(np.nonzero(col))

        row = np.sum(labels_mask, 1)
        x0 = np.min(np.nonzero(row))
        x1 = np.max(np.nonzero(row))

        #extract feature in original img and write on disk for vizualization
        features.append(img[x0:x1, y0:y1])
        #cv.imwrite(str(id) + "_viz.png", img[x0:x1, y0:y1])
    
    return features

features_1 = extract_features(mask1, img1)
features_2 = extract_features(mask2, img2)

#check how many "logo features" match between the two images as a mean of comparison
def imgMatch(f0, f1):
    matchs = 0
    for i in tqdm(f0):
        if np.sum(i) == 0:
            continue
        
        min_distance = None

        for j in f1:
            #resize the features to match each other size
            f1_resized = cv.resize(j, (i.shape[1], i.shape[0]), interpolation= cv.INTER_LINEAR)
            
            #compare features            
            distance = np.absolute((i - f1_resized)/255)
            diff_rate = np.sum(distance) / np.sum(i.astype(np.float32)/255)

            if min_distance == None or diff_rate < min_distance:
                min_distance = diff_rate            
        
        if min_distance < 0.3:
            matchs += 1
            cv.imwrite("matchs/" + str(matchs) + ".png", i)
        else:
            cv.imwrite("no_matchs/" + str(matchs) + ".png", i)
    
    return matchs

#check how many "text features" match between the two images as a mean of comparison
model = gensim.downloader.load('glove-twitter-25')
def txtMatch(f0, f1):
    reader = easyocr.Reader(['ch_sim','en']) #...
    
    matchs = 0
    for i in tqdm(f0):
        
        if np.sum(i) == 0:
            continue

        #read feature 1       
        cv.imwrite("temp.png", i)     
        txts1 = reader.readtext("temp.png")  
        
        if len(txts1) == 0:
            continue

        min_distance = None

        for j in f1:
            #resize the features to match each other size
            f1_resized = cv.resize(j, (i.shape[1], i.shape[0]), interpolation= cv.INTER_LINEAR)
            
   
            #read feature 2
            cv.imwrite("temp.png", j)     
            txts2 = reader.readtext("temp.png")

            if len(txts2) == 0:
                continue
            t1 = ""
            for k in txts1:
                t1 += k[1].lower()  

            t1_vec = np.array([])
            for k in word_tokenize(t1):
                if k not in model.index_to_key:
                    continue

                if len(t1_vec) == 0:
                    t1_vec = np.copy(model[k])
                else:
                    t1_vec += np.copy(model[k])

            t1_vec /= len(word_tokenize(t1))

            t2 = ""
            for k in txts2:
                t2 += k[1].lower()

            t2_vec = np.array([])
            for k in word_tokenize(t2):
                if k not in model.index_to_key:
                    continue

                if len(t2_vec) == 0:
                    t2_vec = np.copy(model[k])
                else:
                    t2_vec += np.copy(model[k])
            t2_vec /= len(word_tokenize(t2))

            if t1_vec.shape[0] == 0 or t2_vec.shape[0] == 0:
                continue

            if np.sum(t1_vec) == 0 or np.sum(t2_vec) == 0:
                continue

            dist = 1 - cosine_similarity(t1_vec.reshape(1, -1), t2_vec.reshape(1, -1))[0][0]
            
            if min_distance == None or dist < min_distance:
                min_distance = dist
        
        if min_distance != None and min_distance < 0.15:
            matchs += 1
            cv.imwrite("matchs/txt_" + str(matchs) + ".png", i)
        else:
            cv.imwrite("no_matchs/txt_" + str(matchs) + ".png", i)
            
    
    return matchs

m1 = imgMatch(features_1, features_2)
m2 = imgMatch(features_2, features_1)

m1 += txtMatch(features_1, features_2)
m2 += txtMatch(features_2, features_1)

print(m1+m2/2, " / ", len(features_1) + len(features_2) / 2, " features match !")